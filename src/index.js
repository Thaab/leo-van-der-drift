function menuToggle() {
  var x = document.getElementById("menu-links");
  if (x.style.visibility === "hidden") {
    x.style.visibility = "unset";
    x.style.height = "100%";
    x.style.transform = "rotateX(0)";
  } else {
    x.style.visibility = "hidden";
    x.style.height = "0";
    x.style.transition = "0.5s";
    x.style.transform = "rotateX(90deg)";
  }
}
